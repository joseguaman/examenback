var express = require('express');
var router = express.Router();
const { body } = require('express-validator');
const RolController = require('../controls/RolController');
var rolController = new RolController();
const PersonaController = require('../controls/PersonaController');
var personaController = new PersonaController();
const CuentaController = require('../controls/CuentaController');
var cuentaController = new CuentaController();

const SitioController = require('../controls/SitioController');
var autoController = new SitioController();

let jwt = require('jsonwebtoken');


//middleware
var auth = function middleware(req, res, next) {
  console.log('auth !!!');
  const token = req.headers['x-api-token'];
  //console.log(next);
  //console.log(req.headers);
  //console.log(token);
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        //console.log(err);
        res.status(401);
        res.json({ msg: "Token no valido o expirado!", code: 401 });
      } else {
        var models = require('../models');
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({ where: { external_id: req.decoded.external } });
        if (aux === null) {
          res.status(401);
          res.json({ msg: "Token no valido!", code: 401 });
        } else {
          next();
        }
      }
    });
  } else {
    res.status(401);
    res.json({ msg: "No existe token!", code: 401 });
  }
}
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});
//CUENTA
router.post('/sesion', [
  body('email', 'Ingrese un correo valido!').trim().exists().not().isEmpty().isEmail(),
  body('clave', 'Ingrese la clave').trim().exists().not().isEmpty()
], cuentaController.sesion);
//FIN CUENTA
router.get('/roles', auth, rolController.listar);
//personas
router.post('/personas/cliente/guardar', [
  body('apellidos', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 100 }).withMessage("Ingrese un valor mayor o igual a 3 y menor a 100"),
  body('nombres', 'Ingrese algun dato').trim().exists()
], personaController.guardarCliente);

router.post('/personas/guardar',  [
  body('apellidos', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 100 }).withMessage("Ingrese un valor mayor o igual a 3 y menor a 100"),
  body('nombres', 'Ingrese algun dato').trim().exists(),
  body('email', 'Ingrese algun dato').trim().exists().isEmail().withMessage("Ingrese un correo valido"),
  body('external_rol', 'Ingrese algun dato').trim().exists(),
  body('tipo', 'Ingrese algun dato CEDULA, RUC, PASAPORTE').trim().exists().not().isEmpty(),
  body('dni', 'Ingrese algun dato').trim().exists().isNumeric().withMessage("Ingrese un dato numerico"),
  body('direccion', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 2, max: 100 }).withMessage("Ingrese un valor mayor o igual a 2 y menor a 200"),
], personaController.guardar);
router.post('/personas/modificar', auth, personaController.modificar);
router.get('/personas', auth, personaController.listar);
router.get('/personas/obtener/:external', auth, personaController.obtener);

//sitio
router.post('/sitios/guardar', auth, [
  body('nombre', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 2, max: 100 }).withMessage("Ingrese un valor mayor o igual a 2 y menor a 100"),
  body('longitud', 'Ingrese algun dato').trim().exists().not().isEmpty().isNumeric().withMessage("Ingrese un valor numerico valido"),
  body('latitud', 'Ingrese algun dato').trim().exists().not().isEmpty().isNumeric().withMessage("Ingrese un valor numerico valido"),
  body('anio', 'Ingrese algun dato').trim().exists().not().isEmpty().isNumeric().withMessage("Ingrese un valor numerico valido"),
  body('descripcion', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 200 }).withMessage("Ingrese un valor mayor o igual a 3 y menor a 200"),
], autoController.guardar);

router.post('/sitios/galeria/guardar', auth, autoController.guardarGaleria);

router.get('/sitios', auth, autoController.listar);
router.get('/sitios/galeria/:external', auth, autoController.listar_galeria);

module.exports = router;