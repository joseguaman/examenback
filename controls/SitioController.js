'use strict';
var models = require('../models');
const { validationResult } = require('express-validator');
var sitio = models.sitio;
var galeria = models.galeria;
var uuid = require('uuid');
var fs = require('fs');
/**
 * Manejo de formularios
 */
var formidable = require('formidable');
/**
 * Tipo de formatos de imagen
 */
var extensiones = ["jpg", "png", "gif", "jpeg"];
/**
 * Tamano de la imagen maximo 1 MB
 */
var maxSize = 1 * 1024 * 1024;

class SitioController {

    async listar_galeria(req, res) {
        const external = req.params.external;
        var sitio1 = await sitio.findOne({where: { external_id: external }});
        if(sitio1) {
            var lista = await galeria.findAll({
                where: { id_sitio: sitio1.id },
                attributes: ['nombre', 'external_id', 'tipo']
            });
            res.status(200);
            res.json({ msg: "OK!", code: 200, info: lista });
        }else {
            res.status(400);
            res.json({ msg: "No hay sitio", code: 400});
        }
        
    }

    async listar(req, res) {
        var lista = await sitio.findAll({
            attributes: ['nombre', 'external_id', 'longitud', 'latitud', 'descripcion', 'anio']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista });
    }
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {

            var data = {
                nombre: req.body.nombre,
                longitud: req.body.longitud,
                latitud: req.body.latitud,
                external_id: uuid.v4(),
                anio: req.body.anio,
                descripcion: req.body.descripcion
            };
            var result = await sitio.create(data);
            if (result === null) {
                res.status(400);
                res.json({ msg: "No se ha registrado el sitio", code: 400 });
            } else {
                res.status(200);
                res.json({ msg: "Se ha registrado el sitio", code: 200 });
            }
        }
        else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    guardarGaleria(req, res) {
      //  let errors = validationResult(req);
      //  if (errors.isEmpty()) {
            var form = new formidable.IncomingForm(), files = [];
            var band = false;
            form
                .on('file', function (field, file) {
                    files.push(file);
                })
                .on('end', function () {

                });
            form.parse(req, function (err, fields) {
                
                var listado = files;
                //console.log("hola");
                console.log(fields.external);
                //console.log(listado);
                var body = {"external":fields.external[0]};//JSON.parse(fields);
                var cont = 0;
                for (var index = 0; index < listado.length; index++) {
                    (function (j) {
                        var file = listado[j];
                        if (file.size <= maxSize) {
                            var extension = file.originalFilename.split(".").pop().toLowerCase();
                            if (extensiones.includes(extension)) {
                                var external = uuid.v4();
                                var nombreFoto = external + "." + extension;
                                fs.rename(file.filepath, "public/images/uploads/" + nombreFoto,
                                    async function (err) {

                                        if (err) {
                                            res.status(200);
                                            res.json({ msg: "No se pudo guardar el archivo en el servidor", code: 400, errors: err });
                                            band = true;
                                        } else {
                                            var marcaA = await sitio.findOne({ where: { external_id: body.external } });
                                            if (marcaA === null) {
                                                res.status(400);
                                                res.json({ msg: "No existe el sitio", code: 400 });
                                            } else {
                                                var data = {
                                                    nombre: nombreFoto,
                                                    tipo: "img",
                                                    estado: true,
                                                    external_id: external,
                                                    anio: body.anio,
                                                    idioma: "sp",
                                                    id_sitio: marcaA.id
                                                };
                                                var result = await galeria.create(data);
                                                if (result === null) {
                                                    res.status(400);
                                                    res.json({ msg: "No se ha registrado la imagen", code: 400 });
                                                } else {
                                                    res.status(200);
                                                    res.json({ msg: "Se ha registrado la imagen", code: 200 });
                                                }
                                            }
                                        }
                                    });
                            } else {
                                res.status(200);
                                res.json({ msg: "El archivo debe ser png, jpeg, jpg o jpg", code: 400 });
                                band = true;
                            }

                        } else {
                            res.status(200);
                            res.json({ msg: "La imagen debe pesar maximo 1 MB", code: 200 });
                            band = true;
                        }
                    }
                        (index));
                }
            });
      //  } else {
      //      res.status(400);
      //      res.json({ msg: "Datos faltantes", code: 400, errors: errors });
      //  }
    }

}
module.exports = SitioController;
