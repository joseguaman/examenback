'use strict';
const { validationResult } = require('express-validator');
var models = require('../models/');
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const saltRounds = 8;
let jwt = require('jsonwebtoken');
class CuentaController {
    async sesion(req, res, next) {
    	console.log(req);
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var login = await cuenta.findOne({
                where: {correo: req.body.email},
                include: { model: models.persona, as: "persona", attributes: ['apellidos', 'nombres'] }
            });
            if (login === null) {
                res.status(400);
                res.json({ msg: "Cuenta no encontrada", code: 400 });
            } else {
                res.status(200);
                var isClaveValida = function(clave, claveUser){
                    return bcrypt.compareSync(claveUser, clave);
                };
                if(login.estado){
                    if(isClaveValida(login.clave, req.body.clave)){
                        const tokenData = {
                            external: login.external_id,
                            email: login.correo,
                            check:true
                        };
                        require('dotenv').config();
                        const llave = process.env.KEY;
                        const token = jwt.sign(tokenData, llave, {
                            expiresIn: '12h'
                        });
                        var info = {
                            token: token,
                            user: login.persona.nombres + ' '+login.persona.apellidos,
                            correo: login.correo
                        };
                        res.json({
                            msg: "OK!",
                            info: info,
                            code: 200
                        });
                    } else {
                        res.json({ msg: "Clave incorrecta!", code: 200 });    
                    }
                } else {
                    res.json({ msg: "Cuenta desactivada!", code: 200 });
                }
                
                //res.json({ msg: "Se ha modificado sus datos", code: 200 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
}
module.exports = CuentaController;
