-- MariaDB dump 10.19  Distrib 10.11.6-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: examendb
-- ------------------------------------------------------
-- Server version	11.1.2-MariaDB-1:11.1.2+maria~ubu2204

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cuenta`
--

DROP TABLE IF EXISTS `cuenta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuenta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `correo` varchar(100) DEFAULT 'NO_DATA',
  `external_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `clave` varchar(120) NOT NULL,
  `estado` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `id_persona` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `correo` (`correo`),
  KEY `id_persona` (`id_persona`),
  CONSTRAINT `cuenta_ibfk_1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuenta`
--

LOCK TABLES `cuenta` WRITE;
/*!40000 ALTER TABLE `cuenta` DISABLE KEYS */;
INSERT INTO `cuenta` VALUES
(2,'admin@admin.com','c4c19b00-d0e7-4b5a-b0a9-7572298de75d','$2b$08$yYwgKWOmjKGebfkz5pUotuTciGB6Mecj7gGIgxt0IVKtJMvQhRpdm',1,'2024-06-20 13:57:48','2024-06-20 13:57:48',4);
/*!40000 ALTER TABLE `cuenta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galeria`
--

DROP TABLE IF EXISTS `galeria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galeria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `tipo` enum('img','video','file') DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `idioma` varchar(50) DEFAULT NULL,
  `external_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `id_sitio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_sitio` (`id_sitio`),
  CONSTRAINT `galeria_ibfk_1` FOREIGN KEY (`id_sitio`) REFERENCES `sitio` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galeria`
--

LOCK TABLES `galeria` WRITE;
/*!40000 ALTER TABLE `galeria` DISABLE KEYS */;
/*!40000 ALTER TABLE `galeria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) DEFAULT 'NO_DATA',
  `apellidos` varchar(100) DEFAULT 'NO_DATA',
  `identificacion` varchar(20) DEFAULT 'NO_DATA',
  `tipo_identificacion` enum('CEDULA','PASAPORTE','RUC') DEFAULT 'CEDULA',
  `external_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `direccion` varchar(255) DEFAULT 'NO_DATA',
  `estado` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `id_rol` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identificacion` (`identificacion`),
  KEY `id_rol` (`id_rol`),
  CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES
(4,'Administrador','Admin','2222222222','CEDULA','1251c115-9792-403b-859d-77dde747bbd2','lauro guerrero',1,'2024-06-20 13:57:48','2024-06-20 13:57:48',1);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) DEFAULT NULL,
  `external_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `estado` tinyint(1) DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES
(1,'admin','79691a59-2eac-11ef-a332-b48c9d8e7c23',1,'2024-06-20 12:56:15','2024-06-20 12:56:15'),
(2,'visit','8006e587-2eac-11ef-a332-b48c9d8e7c23',1,'2024-06-20 12:56:26','2024-06-20 12:56:26');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sitio`
--

DROP TABLE IF EXISTS `sitio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sitio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT 'NO_DATA',
  `longitud` double DEFAULT 0,
  `latitud` double DEFAULT 0,
  `descripcion` varchar(20) DEFAULT 'NO_DATA',
  `external_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `anio` int(11) DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sitio`
--

LOCK TABLES `sitio` WRITE;
/*!40000 ALTER TABLE `sitio` DISABLE KEYS */;
INSERT INTO `sitio` VALUES
(1,'Identidad Lojana I, II,III',-79.205085,-3.977388,'Linda esta','24837d99-2eb5-11ef-a332-b48c9d8e7c23',1900,'2024-06-20 14:06:42','2024-06-20 14:06:42'),
(2,'Marcelino pan y vino',-79.19757,-3.970803,'Linda esta','49c0b726-2eb5-11ef-a332-b48c9d8e7c23',1980,'2024-06-20 14:07:45','2024-06-20 14:07:45'),
(3,'Las enseñanzas de la  biblia    y  los jóvenes',-79.201231,-3.989155,'Linda esta','6b0126e8-2eb5-11ef-a332-b48c9d8e7c23',1990,'2024-06-20 14:08:40','2024-06-20 14:08:40'),
(4,'vendedoras',-79.204384,-3.989017,'Linda esta','edf35894-2eb5-11ef-a332-b48c9d8e7c23',2012,'2024-06-20 14:12:20','2024-06-20 14:12:20'),
(5,'Juan de Salinas y Loyola',-79.204193,-3.989742,'Linda esta','e468d411-2eb6-11ef-a332-b48c9d8e7c23',1999,'2024-06-20 14:19:14','2024-06-20 14:19:14'),
(6,'Antonio José de Sucre',-79.204234,-3.989935,'Linda esta','feb50442-2eb6-11ef-a332-b48c9d8e7c23',1999,'2024-06-20 14:19:58','2024-06-20 14:19:58'),
(7,'En algun lugar',-79.204234,-4.0001,'Linda esta','489cde91-2eb7-11ef-a332-b48c9d8e7c23',1987,'2024-06-20 14:22:02','2024-06-20 14:22:02'),
(8,'Cas esta lista',-79.204234,-4.0109,'Linda esta','5227d1c0-2eb7-11ef-a332-b48c9d8e7c23',1987,'2024-06-20 14:22:18','2024-06-20 14:22:18'),
(9,'La casona vieja',-79.207372,-4.028139,'prueba','ead53f42-ffab-41f5-a36f-21d7bb97c8f5',2012,'2024-06-20 14:27:14','2024-06-20 14:27:14');
/*!40000 ALTER TABLE `sitio` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-20 11:36:15
