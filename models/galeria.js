'use strict';
module.exports = (sequelize, DataTypes) => {
  const galeria = sequelize.define('galeria', {
    nombre: DataTypes.STRING(50), 
    tipo: DataTypes.ENUM('img', 'video', 'file'), 
    estado: DataTypes.BOOLEAN,
    idioma: DataTypes.STRING(50),
    external_id: DataTypes.UUID
  }, {freezeTableName: true});
  galeria.associate = function(models) {
    // associations can be defined here
   // persona.hasOne(models.cuenta, {foreignKey: 'id_persona', as: 'cuenta'});
    galeria.belongsTo(models.sitio, {foreignKey: 'id_sitio'});
    
  };
  return galeria;
};
