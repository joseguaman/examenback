'use strict';
module.exports = (sequelize, DataTypes) => {
    const sitio = sequelize.define('sitio', {
        nombre: { type: DataTypes.STRING(200), defaultValue: "NO_DATA" },
        longitud: { type: DataTypes.DOUBLE, defaultValue: 0.0 },
        latitud: { type: DataTypes.DOUBLE, defaultValue: 0.0 },        
        descripcion: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        anio: { type: DataTypes.INTEGER, defaultValue: 0 }    
        
    }, { freezeTableName: true });
    sitio.associate = function(models) {
        sitio.hasMany(models.galeria, {foreignKey: 'id_sitio', as: 'galeria'});        
      };
    
    
    return sitio;
};